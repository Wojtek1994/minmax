Author: Wojciech Wandzik

Project description:
Program analyses position in a simple game using minmax algorithm with alpha-beta pruning. There are two DLLs implementing game logic that may be loaded to the project. The position may be analysed with different number of threads. More details in: Dokumentacja/Karta_projektu.pdf.

It is required to have .NET Framework 4.5 or higher installed. Link to official download site: https://www.microsoft.com/pl-pl/download/details.aspx?id=30653