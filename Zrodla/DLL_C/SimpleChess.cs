﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;


// Author: Wojciech Wandzik
// DDL library - performs game logic

namespace SimpleChessCS
{
    public static class SimpleChess
    {


        public static IntPtr Analyse(byte[][] position, byte rows, byte cols, bool onMove, ref int counter)
        {
            int direction, pawnValue, begin, end, evaluation, opponentPawn;
            counter = 0;
            evaluation = CheckPromotion(position);
            if (evaluation != 0)
            {
                return (IntPtr)evaluation;
            }
            evaluation = CheckLeftPawns(position);
            if (evaluation != 0)
            {
                return (IntPtr)evaluation;
            }

            // the position cannot be evaluated so far -> find possible moves
            byte[] moves = new byte[80];          // assuming max number of moves: 20
            if (onMove)
            {
                pawnValue = 1;
                direction = 1;
                begin = 0;
                end = rows - 1;
            }
            else
            {
                pawnValue = 2;
                direction = -1;
                begin = rows - 1;
                end = 0;
            }
            opponentPawn = pawnValue + direction;
            for (byte i = (byte)begin; i != end; i = (byte)(i + direction))
            {
                for (byte j = 0; j < position[0].Length; j++)
                {
                    if (position[i][j] == pawnValue)
                    {
                        i = (byte)(i + direction);       // the destination row is row + direction
                        if (position[i][j] == 0)
                        {
                            // move forward possible (0 means empty field)
                            moves[counter++] = (byte)(i - direction);
                            moves[counter++] = j;
                            moves[counter++] = i;
                            moves[counter++] = j;
                        }
                        if (j != 0)
                        {
                            // check if there is opponent's pawn across
                            if (position[i][j - 1] == opponentPawn)
                            {
                                // capture opponent's pawn possible
                                moves[counter++] = (byte)(i - direction);
                                moves[counter++] = j;
                                moves[counter++] = i;
                                moves[counter++] = (byte)(j - 1);
                            }
                        }
                        if (j != position[0].Length - 1)
                        {
                            // check if there is opponent's pawn across
                            if (position[i][j + 1] == opponentPawn)
                            {
                                // capture opponent's pawn possible
                                moves[counter++] = (byte)(i - direction);
                                moves[counter++] = j;
                                moves[counter++] = i;
                                moves[counter++] = (byte)(j + 1);
                            }
                        }
                        i = (byte)(i - direction);
                    }
                }
            }

            if (counter == 0)
            // no moves found and position couldnt be evaluated = return 0
            {
                return IntPtr.Zero;
            }
            IntPtr result = Marshal.AllocCoTaskMem(counter);
            Marshal.Copy(moves, 0, result, counter);
            counter /= 4;           // number of moves = number of bytes / 4 = counter / 4
            return result;
        }



        private static void CheckPawnMoves(byte[][] position, int row, int col, int direction, int pawn, List<byte> moves)
        {
            int opponentPawn = pawn + direction;

            row += direction;       // the destination row is row + direction
            if (position[row][col] == 0)
            {
                // move forward possible (0 means empty field)
                moves.Add((byte)(row - direction));
                moves.Add((byte)col);
                moves.Add((byte)row);
                moves.Add((byte)col);
            }
            if (col != 0)
            {
                // check if there is opponent's pawn across
                if (position[row][col - 1] == opponentPawn)
                {
                    // capture opponent's pawn possible
                    moves.Add((byte)(row - direction));
                    moves.Add((byte)col);
                    moves.Add((byte)row);
                    moves.Add((byte)(col - 1));
                }
            }
            if (col != position[0].Length - 1)
            {
                // check if there is opponent's pawn across
                if (position[row][col + 1] == opponentPawn)
                {
                    // capture opponent's pawn possible
                    moves.Add((byte)(row - direction));
                    moves.Add((byte)col);
                    moves.Add((byte)row);
                    moves.Add((byte)(col + 1));
                }
            }
        }



        private static int CheckPromotion(byte[][] position)
        {
            byte sum = 0;
            byte[] subArr = position[0];
            for (int i = 0; i < subArr.Length; i++)
            {
                sum |= subArr[i];
            }
            if ((sum & 2) != 0)
            {
                return -1;
            }

            sum = 0;
            subArr = position[position.Length - 1];
            for (int i = 0; i < subArr.Length; i++)
            {
                sum |= subArr[i];
            }
            if ((sum & 1) != 0)
            {
                return 1;
            }
            return 0;
        }


        private static int CheckLeftPawns(byte[][] position)
        {
            byte pawns = 0;
            foreach (byte[] row in position)
            {
                foreach (byte b in row)
                {
                    // if there is 1 in array - then pawns bit 0 will be 1
                    // if there is 2 in array - then pawns bit 1 will be 1
                    pawns |= b;
                }
            }
            if (pawns == 1)
            {
                // only white pawn found
                return 1;
            }
            else
            {
                if (pawns == 2)
                {
                    // only black pawn found
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }


        public static bool CheckMove(byte[][] position, byte rows, byte cols, byte y0, byte x0, byte y1, byte x1, bool onMove)
        {
            if (CheckPromotion(position) != 0)
            {
                return false;
            }
            int pawnValue, direction, oppValue;
            if (onMove)
            {
                pawnValue = 1;
                direction = 1;
            }
            else
            {
                pawnValue = 2;
                direction = -1;
            }
            oppValue = pawnValue + direction;
            if (position[y0][x0] != pawnValue || y0 + direction != y1 || Math.Abs(x0 - x1) > 1)
                // if on the source field is no correct pawn or the distance (in rows) is != 1
                // or the distance (in cols) is > 1 then return false
            {
                return false;
            }
            if (x0 == x1)
            {
                // move forward (there must be empty field)
                if (position[y1][x1] == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                // move across (there must be opponents pawn)
                if (position[y1][x1] == oppValue)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }
}
