; Author: Wojciech Wandzik
; DDL library - performs game logic

.386
.MODEL FLAT, STDCALL		; using stdcall calling convention

OPTION CASEMAP:NONE

.code

; library ole32 is used to allocate COM memory. (procedure CreateTable)
INCLUDE C:\masm32\include\ole32.inc
INCLUDELIB C:\masm32\lib\ole32.lib



; the entry of the dll. It should return TRUE (1)
DllEntry PROC hInstDLL:DWORD, reason:DWORD, reserved1:DWORD

    mov eax, 1
    ret

DllEntry ENDP



;----------------------------------------------------------------------------------------------------
; CheckPromotion checks if any black pawns is already on the 0 row or any of white pawns is on the last row
; if there is any, then the position evaluation is the return value (-1 or 1). if there is none - then 0 is returned
CheckPromotion proc Position: DWORD, Rows: BYTE, Cols: BYTE

	mov EBX, Position		; loading addresses of rows in 2D array to EBX register
	mov EBX, [EBX]			; loading address of first row in 2D array
	movzx ECX, Cols			; save value of Cols in ECX register (i.e. Cols == 5 then EXC == 0x00000005)

	; register CL is now a 'loop' variable - it will be decremented at the beginning of each iteration
	; so its values will be Cols - 1, Cols - 2 and so on. When Cols == 0 the whole row in 2D array is iterated.

BlackPromotion:
	dec CL			; decrementing CL (currently analysed array column)
	test BYTE PTR [EBX + ECX], 02h		; check if in Position[0][CL] is black pawn (02h)
	jnz BlackWon			; if black pawn is on the row 0, then black won!
	test CL, CL				; check if it was last iteration (CL == 0)
	jnz BlackPromotion		; if it was not then check next Column in the 0 row

	; no black pawn was on the 0 row in 2D array. Now checking if white is on the last row of the array

	movzx EAX, Cols		; EAX = cols
	mul Rows			; EAX = cols * rows
	sub AL, Cols		; EAX = cols * (rows - 1) - this is the value in bytes that should be added to EBX
						; register, so that it points to the last row in the array instead of the first one.
	add EBX, EAX	; now EBX points to the last row in the array
	mov CL, Cols	; again, setting CL as the 'loop' variable for iterating each column in the last row

WhitePromotion:
	dec CL		; CL is the currently analysed column in the last row
	test BYTE PTR [EBX + ECX], 01h		; check if in Position[last row][CL] is white pawn (01h)
	jnz WhiteWon		; if so, then white won
	test CL, CL			; if not, then check if it was last iteration (CL == 0)
	jnz WhitePromotion	; if it was not last iteration, then go to the next one

NobodyWon:
	mov EAX, 0			; if the position is not possible to evaluate taking into account only promotions
						; then the return value is 0 - meaning that there is no promotion
	ret

BlackWon:
	mov EAX, -1			; if black won then the position evaluation is -1
	ret

WhiteWon:
	mov EAX, 1			; if white won then the position evaluation is 1
	ret

CheckPromotion endp
;----------------------------------------------------------------------------------------------------



;----------------------------------------------------------------------------------------------------
; checks if both colors still have any pawns. If not - then the position may be evaluated as a win of
; one of the sides
CheckLeftPawns proc Position: DWORD, Rows: BYTE, Cols: BYTE

	mov EBX, Position		; EBX points to the address of the first row in the table
	mov EBX, [EBX]			; EBX points to the first element in the first row in the table
	movzx EAX, Rows		; mov number of rows to AL and zeros to the rest of EAX register (zero-extends)
	mul Cols			; EDX:EAX = Rows * Cols (number of array elements). In fact, EDX = 0, because 
						; both Rows and Cols are not a big numbers
	mov ECX, EAX	; save number of elements to ECX register
	mov EAX, 00h	; clear EAX

	; ECX (number of elements in array) will be the 'loop' variable
	; currently EAX == 0. in each iteration there will be a bit sum with consecutive array elements.
	; if the field is 'empty' it will be 00 | 00 = 00. If there will be white pawn then x0 | 01 = x1
	; if there will be black pawn, then 0x | 10 = 1x. If the value of AL is 11b then there are both colors
	; pawns found. If not, then AL stores value of the pawn that was found. If AL == 0 then no pawn was found.

CheckField:
	dec ECX		; decrease ECX in each iteration
	or AL, BYTE PTR [EBX + ECX]		; or AL with next element in array
	test ECX, ECX		; check if this is last iteration (ECX == 0)
	jnz CheckField		; if not, then iterate again
	
	cmp AL, 03h			; check if both pawns were found
	jz BothColorsLeft	; if so, then jump and return 0

OneColorLeft:
	and AL, 01h			; check if the only left pawn is white (AL & 01h)
	jz BlackWon			; if not, then black won (return -1)
	ret					; if so, then just return 01h from EAX register

BlackWon:
	mov EAX, -1			; black won = return -1
	ret

BothColorsLeft:
	mov EAX, 0			; nobody won, so return 0
	ret

CheckLeftPawns endp
;----------------------------------------------------------------------------------------------------




;----------------------------------------------------------------------------------------------------
; Procedure create table does the following:
; - pops from the stack number of elements (counter)
; - allocates memory for 4 * counter bytes
; - pops from stack 'counter' times 4 bytes and saves them in the memory
; - returns a pointer to allocated memory
; The procedure has all arguments on the stack, it is written in cdecl calling convention
; It uses multiple regiesters but it is called only in Analyse procedure
; so it is not needed to specify it in USES section
CreateTable proc

		; creating new stack frame
	push EBP		; saves the base pointer of caller
	mov EBP, ESP	; new stack frame (first argument is esp + 8)
	
	mov EBX, [EBP + 08h]		; get first argument (number of array elements)
	test EBX, EBX		; if counter == 0, then..
	jz NoElements		; jump and return 0
	mov EAX, 04h		; EAX = 4
	mul EBX				; EAX = 4 * counter (this is total number of bytes to allocate for the whole array)
	invoke CoTaskMemAlloc, EAX		; allocating needed bytes, address to them is stored in EAX
	test EAX, EAX		; check return value (if EAX == 0 - null returned)
	jz NoElements		; if CoTaskMemAlloc returns 0 (no memory left, then return null (0))
	mov ECX, EBX		; save counter to ECX (loop variable)
	mov EBX, EAX		; save address of allocated memory in EBX
	mov ESI, 0ch		; save offset 12 to ESI (EBP + 12 is the next argument on the stack)
	sub EBX, ESI		; now, EBX + 12 is the address to the allocated memory
RewriteFromStack:
	mov EAX, [EBP + ESI]	; load to EAX value from the stack
	mov [EBX + ESI], EAX	; save the value from the stack in the allocated memory
	add ESI, 04h		; ESI = ESI + 4 (next 4 bytes)
	loop RewriteFromStack		; rewrite 'counter' times

	mov EAX, EBX		; mov (address of allocated memory - 12) to EAX
	add EAX, 0ch		; get address of allocated memory (EAX + 12)
	pop EBP		; get the caller EBP from the stack (restore caller stack frame)
	ret			; return address of allocated memory (with rewritten values from the stack)
NoElements:
	mov EAX, 0		; program flow comes here if 1. there were no elements on the stack
					; or 2. memory could not be allocated.
					; in both cases null should be returned.
	pop EBP		; pop caller EBP (restore stack frame).
	ret

CreateTable endp
;----------------------------------------------------------------------------------------------------





;----------------------------------------------------------------------------------------------------
; procedure analyses 2D array (Position) of 'Rows' rows and 'Cols' columns. If OnMove != 0 then
; white is on move, otherwise black. MovesCounter is an address of allocated 4 bytes in memory.
; Under this address should be written the number of possible moves.
; If *MovesCounter == 0 then in EAX should be stored the position evaluation (-1, 0 or 1)
; If *MovesCounter > 0 then in EAX should be stored address of allocated memory for possible moves array
; The allocated array is of size 4 * number of possible moves (in bytes)
Analyse proc uses EBX ESI Position: DWORD, Rows: BYTE, Cols: BYTE, OnMove: BYTE, MovesCounter: DWORD

	;------------- locals section

	LOCAL Direction: BYTE		; direction of pawns moves (for white: +1, for black: -1)
	LOCAL CurrentRow: BYTE		; variable storing currently analysed row in 2D array
	LOCAL EndRow: BYTE			; EndRow is the row, where white (EndRow = Rows - 1) or black (EndRow = 0)
								; pawns cannot be found. If they are on the this row, the game is already finished.
	LOCAL PawnValue: BYTE		; 01h for white and 02h for black
	LOCAL CurrentCol: BYTE		; currently analysed column
	LOCAL ESPValue: DWORD		; value of stack pointer (used to restore the ESP value at the end of procedure)
	LOCAL Counter: DWORD		; counter of possible moves. Increased every time the new move is found

	;------------- END of locals

	mov Counter, 0		; setting initial value of counter to 0

	invoke CheckPromotion, Position, Rows, Cols		; check if the game is already finished (by promotion)
	test EAX, EAX		; check if EAX == 0
	jnz EndOfGame		; if not equal to 0, then the position is already evaluated
	invoke CheckLeftPawns, Position, Rows, Cols		; check if both sides still have any pawns
	test EAX, EAX		; check return value (if EAX == 0 then the game is still not evaluated)
	jnz EndOfGame		; if EAX != 0 then position is evaluated
	
	mov ESPValue, ESP	; saving ESP because every found move will be placed on the stack,
						; so at the end ESP value should be restored

	mov AL, OnMove		; check who is on move to set initial values of local variables (i.e. Direction)
	test AL, AL			; check if AL == 0
	jz BlackOnMove		; if so then black is on move, otherwise - white

WhiteOnMove:
; setting initial values if white is on move
	mov PawnValue, 1	; White pawn value = 01h
	mov Direction, 1	; White pawns move direction = +1
	mov CurrentRow, 0	; Currently analysed row will be 0 (first white row)
	mov AL, Rows		; AL = Rows
	dec AL				; AL = Rows - 1
	mov EndRow, AL		; EndRow for white is the row that will not be analysed (this is the last - promotion row)
	jmp NextRow			; jump to analysing rows in the array

BlackOnMove:
; setting initial values when black is on move
	mov PawnValue, 2	; black pawn value is 02h
	mov Direction, -1	; black pawns move direction = -1
	mov AL, Rows		; AL = Rows
	dec AL				; AL = Rows - 1
	mov CurrentRow, AL	; currently analysed row will be Rows - 1 (first black row, the last one in the array)
	mov EndRow, 0		; the end row for black is 0 (that one will not be analysed due to promotion)


NextRow:
; here starts analysis of the rows in the table
	mov AL, Cols		; AL = Cols
	mov CurrentCol, AL	; CurrentCol is set to Cols value (it will be decremented in the loop)
	mov EBX, Position	; EBX points now to the address where the address of the first element in array is stored
	mov EAX, 04h		; EAX = 4
	mul CurrentRow		; EAX = 4 * current row
	add EBX, EAX		; EBX = Position + 4 * current row, that means Position[4 * current row]
						; so now EBX points to the address where the address of current row is stored
	mov EBX, [EBX]		; now EBX points to the first element in 'current row'
	SearchInRow:
	; if EBX is correctly set, it is possible to iterate this row by columns
		dec CurrentCol		; decreasing current col (now it is the last column in the row)
		movzx ESI, CurrentCol		; store current col value (an index in array) in indexing register
		mov AL, BYTE PTR [EBX + ESI]	; read from Position[current row][current col]
		cmp PawnValue, AL		; check if the right pawn is found (white or black depending on pawn value)

		jnz FieldAnalysed	; if the right pawn was not found then the field Position[current row][current col]
							; may be claimed to be analysed
		
		; if the pawn was found then the 3 following options has to be checked:
		; 1. go one field forward (this field has to be empty)
		; 2. go across (to the lower column index) and take the opponent's pawn
		; 3. go across (to the higher column index) and take the opponent's pawn

		; All the moves has the row = current row + direction of moves

		mov AL, CurrentRow	; AL = current row
		add AL, Direction	; AL = next row (added direction of moves)
		mov CurrentRow, AL	; currently analysed row is the next row (where is the move destination)

		mov AL, 04h			; AL = 4
		mul CurrentRow		; AL = 4 * current row
		mov EDX, Position	; EDX = Position
		mov EDX, [EDX + EAX]	; EDX = Position + EAX, that means EDX = Position[current row]

		; now EDX stores address of move destination row. The following fields needs to be checked:
		; 1. if EDX[current col] == 0
		; 2. if EDX[current col - 1] == opponent's pawn
		; 3. if EDX[current col + 1] == opponent's pawn
		; Moreover, it needs to be checked if current col - 1 > 0 and current col + 1 < Cols! (for moves 2. and 3.)

		mov AL, CurrentCol		; AL = current col
		cmp BYTE PTR [EDX + EAX], 00h	; check if EDX[current col] == 0 (empty field) - move no. 1
		jnz MoveNotFound		; if it is not 0 then the first move is not found
		mov CL, CurrentRow		; if it is 0, then the move is stored temporarly on the stack, CL = destination row
		mov CH, CurrentCol		; CH = destination column
		push CX					; push destination field
		sub CL, Direction		; CL = source row (CH is destination column as well as source column)
		push CX					; push source field
		inc Counter				; increase counter of moves


	MoveNotFound:
	; move forward is already analysed, now check move 2. (column = source column - 1)
		cmp CurrentCol, 00h		; if current column is 0, then the move will not be possible at all!
		jz AfterTake1			; then jump to analysing the move 3.
		mov AL, CurrentCol		; AL = current col
		dec AL					; AL = current col - 1 (now check if EDX[EAX] == opponent's pawn)

		mov CL, PawnValue		; for white: CL = 1, for black CL = 2
		add CL, Direction		; for white CL = 1 + 1 = 2, for black CL = 2 + (-1) = 1, so CL = opponent's pawn!
		cmp BYTE PTR [EDX + EAX], CL		; check if the move 2. is possible
		jnz Take1NotFound		; if there is not an opponent's pawn to take then move 2 is not possible
		mov CL, CurrentRow		; otherwise, the move coordinates are pushed on the stack (destination and source)
		mov CH, AL			; CL = destination row, CH = destination col
		push CX				; push destination field
		sub CL, Direction	; CL = source row
		inc CH				; CH = destination column
		push CX				; push destination field
		inc Counter			; increase moves counter

	Take1NotFound:
	; when the jump here is done, then in AL is stored the value of current col - 1
	; to check if the move 3. is possible to be done, then it needs to be equal to current col + 1
		add AL, 02h		; by adding 2 it is current col - 1 + 2 = current col + 1
		cmp AL, Cols	; check if move 3. is in the range of the board
		jz PawnFoundAndAnalysed		; if current col + 1 == number of columns, then the move 3. is not possible
									; and currently found pawn is fully analysed (= jump)

	AfterTake1:
	; check the move 3.
		mov CL, PawnValue			; again, for white CL = 1, for black CL = 2
		add CL, Direction			; now CL = opponents' pawn
		mov AL, CurrentCol			; AL = current col
		inc AL					; AL = current col + 1 (so the destination column of move 3.)
		cmp BYTE PTR [EDX + EAX], CL	; EDX is Position[destination row], so by adding EAX the 
						; Position[dest. row][dest. column] is read and compared to opponent's pawn value
		jnz PawnFoundAndAnalysed	; if there is no opponents pawn, then the field is already analysed (no move 3.)
		mov CH, AL			; if there is opponent's pawn, then the move 3. is found and the coordinates go on the stack
		mov CL, CurrentRow	; CL = destination row and CH = destination column
		push CX				; push destination field
		dec CH				; CH = source column
		sub CL, Direction	; CL = source row
		push CX				; push source field
		inc Counter		; inrease moves counter

	PawnFoundAndAnalysed:
	; if pawn was found then current row was increased by the direction to find all possible moves
	; now, the value of current row should be restored so that: current row = current row - direction
		mov AL, CurrentRow	; AL = current row
		sub AL, Direction	; AL = current row - direction
		mov CurrentRow, AL	; current row = current row - direction
	
	FieldAnalysed:
	; if the field is analysed then it needs to be checked if there is:
	; - next column in the row
	; - if not, then next row in the array,
	; - if not, then this is the end of array and the results should be popped from stack
		mov AL, CurrentCol	; AL = current column
		test AL, AL			; testing if AL == 0 (the last column in the row)
		jnz SearchInRow		; if this is not, then iterate in the row again
	mov AL, CurrentRow	; if it was the last column, then check if it is the last row
	add AL, Direction	; add the direction to get next row
	mov CurrentRow, AL	; save the value of the next row as current row
	cmp AL, EndRow		; check if it was the last row to be analysed in the array
	jnz NextRow			; if this was not the last one then analyse next row
	push Counter		; if it was the last one, then the stack should be copied to the allocated
						; memory and returned as an array. To do this, i call CreateTable with the arguments:
						; CreateTable(move1, move2, ..., counter) (they were pushed right to left)
	call CreateTable	; and call create table - it returns the address of allocated memory!
	mov ESP, ESPValue	; restoring the ESP value (it was messed with pushed moves and counter)

EndOfGame:
	mov EDX, Counter		; saving value of counter to MovesCounter that may be gained by the caller
	mov EBX, MovesCounter	; get address of MovesCounter
	mov [EBX], EDX			; write counter to memory
	ret						; return (there is an address of allocated memory or position evaluation in EAX)

Analyse endp
;----------------------------------------------------------------------------------------------------




;----------------------------------------------------------------------------------------------------
; procedure checks if the move is legal according to:
; - current position on the board (Position)
; - SouY, SouX - source field
; - DestY, DestX - destination field
; - OnMove - if equals to 0 than black on move, otherwise white
CheckMove proc uses EBX ECX Position: DWORD, Rows: BYTE, Cols: BYTE, SouY: BYTE, SouX: BYTE, DestY: BYTE, DestX: BYTE, OnMove: BYTE

LOCAL Direction: BYTE		; direction of move (+1 for white and -1 for black)
LOCAL PawnValue: BYTE		; PawnValue = 1 for white and 2 for black
LOCAL DestinationFieldValue: BYTE		; value stored in Postition[DestY][DestX]

	invoke CheckPromotion, Position, Rows, Cols		; check if game is already over
	jnz IllegalMove									; if it is then any move is illegal

	mov AL, OnMove		; check who is on move
	test AL, AL			; by testing OnMove value
	jz BlackOnMove		; if OnMove == 0 then black on move, otherwise white

WhiteOnMove:
	mov Direction, 1		; Direction for white is +1 (increasing row value)
	mov PawnValue, 1		; White pawn value is 1
	jmp CheckSourceField	; check if there is proper pawn on source field

BlackOnMove:
	mov Direction, -1		; Direction for black is -1 (decreasing row value)
	mov PawnValue, 2		; Black pawn value is 2

CheckSourceField:
	mov EBX, Position		; load 2D array to EBX
	mov EAX, 04h			; EAX = 4
	mul SouY				; EAX = 4 * SouY (now EBX + EAX points to SouY row -> Position[SouY])
	mov EBX, [EBX + EAX]	; load address of the source row to EBX
	mov AL, SouX			; load SouX to AL (in fact EAX because SouY is less than 255)
	mov AL, [EBX + EAX]		; load to AL Position[SouY][SouX]
	test AL, PawnValue	; check if AL == PawnValue (proper pawn on the source field)
	jz IllegalMove		; if not, than the move is illegal (there must be right pawn on source field)

; get the value stored in Position[DestY][DestX] and save it in DestinationFieldValue variable
	mov EBX, Position			; load the address of array to EBX
	mov AL, 04h					; EAX = 4
	mul DestY					; EAX = 4 * DestY (now EBX + EAX points to DestY row - Position[DestY])
	mov EBX, [EBX + EAX]		; get the address of the destination row
	mov AL, DestX				; load destination column to EAX
	mov AL, [EBX + EAX]			; save value from the array destination field in AL..
	mov DestinationFieldValue, AL		; ..and copy it to variable,
										; so that DestinationFieldValue = Position[DestY][DestX]

CheckCoordinates:
; check if the fields (source and destination) are neighbouring
	mov AL, SouY		; AL = source row
	add AL, Direction	; AL = source row + direction = destination row
	cmp AL, DestY		; check if destination row is equal to calculated above
	jnz IllegalMove		; if they are not equal then the move is illegal
	; row is checked. Now check columns values
	mov AL, SouX		; AL == source column
	cmp AL, DestX		; compare source column with destination column
	jz MoveForward		; if they are equal then the fields are neighbouring and the move was 1 field forward
	inc AL			; now AL = source column + 1
	cmp AL, DestX	; check if source column + 1 == destination column
	jz MoveAcross	; if so then the move was across (taking opponent's pawn)
	sub AL, 02h		; now AL = source column - 1
	cmp AL, DestX	; check if source column - 1 == destination column
	jz MoveAcross	; if so then the move was across (taking opponent's pawn)
	jmp IllegalMove	; if none of the 3 above conditions was fulfilled then the move is surely illegal

MoveForward:
; the last thing to check is whether DestinationFieldValue == 0
	mov AL, DestinationFieldValue		; copy the value to AL
	test AL, AL			; and check if it is equal to 0
	jz LegalMove		; if so, then the move is legal
	jmp IllegalMove		; if not - illegal

MoveAcross:
; it needs to be checked, whether DestinationFieldValue is equal to opponent's pawn value.
; the opponent's pawn value is equal to PawnValue + Direction
	mov AL, PawnValue		; AL = PawnValue
	add AL, Direction		; AL = OpponentPawnValue
	test AL, DestinationFieldValue		; check if on destination field is opponent's pawn
	jz IllegalMove			; if they are not equal (OpponentPawnValue and DestinationFieldValue)
							; then the move is illegal
	; otherwise it is legal

LegalMove:
	mov EAX, 1		; if the move is legal, then return 1 (true)
	ret

IllegalMove:
	mov EAX, 0		; if the move is illegal, then return 0 (false)
	ret

CheckMove endp
;----------------------------------------------------------------------------------------------------


END DllEntry 
