﻿
using MinMax.Games.Model;

namespace MinMax.MinMax
{
    class Root : Node
    {

        private byte[] movesArray;      // the array of moves - only for the Root (none of Nodes remember them)

        public byte[] MovesArray
        {
            get { return movesArray; }
            set { movesArray = value; }
        }


        public Root(Game game)
        {
            Children = null;
            Game = game;
            Evaluation = null;

            // the Level value is needed in MinMax algorithm
            // 'even' level = MAX level
            // 'odd' level = MIN level
            if (game.OnMove())
            {
                Level = 0;
            }
            else
            {
                Level = 1;
            }
        }


        public void SaveMoves()
            // the method analyses the game position and saves the possible moves to an array
        {
            MovesArray = Game.Analyse();
        }


    }
}
