﻿using System.Collections.Generic;
using System.Threading;

using MinMax.Games.Model;


namespace MinMax.MinMax
{
    class Node
    {

        public Node()
        {

        }


        public Node(Game game, int level)
            // level should always be defined on the base of parent node (parent node level + 1)
        {
            Children = null;
            Game = game;
            Evaluation = null;
            Level = level;
        }


        private Node[] children;

        public Node[] Children
        {
            get { return children; }
            set { children = value; }
        }


        private int level;

        public int Level
        {
            get { return level; }
            set { level = value; }
        }


        private int? evaluation;

        public int? Evaluation
        {
            get { return evaluation; }
            set { evaluation = value; }
        }


        private Game game;

        public Game Game
        {
            get { return game; }
            set { game = value; }
        }


        public List<ThreadStart> AnalyseWithScheduling(int threads)
           /* the method divides the tree on parts that need approximately even amount of time
            * to be evaluated. Then creates a delegate (ThreadStart) for each of the parts and returns
            * the list with delegates to be called.
            */
        {
            List<ThreadStart> threadStarts = new List<ThreadStart>();
            Analyse();
            if (Children != null)
            {
                int[] ration;
                if (threads > Children.Length)
                {
                    // each node has at least 1 thread
                    ration = Schedule(Children.Length, threads);
                    for (int i = 0; i < Children.Length; i++)
                    {
                        if (ration[i] == 1)
                        {
                            // if node has exactly 1 thread - then the thread may be assigned to the node
                            threadStarts.Add(PrepareAnalyseInSingleThread(i, i));
                        }
                        else
                        {
                            // if node has more than 1 thread than the work should be scheduled recursively
                            threadStarts.AddRange(Children[i].AnalyseWithScheduling(ration[i]));
                        }
                    }
                }
                else
                {
                    // each thread has at least 1 node
                    ration = Schedule(threads, Children.Length);
                    int currentNode = 0;
                    for (int i = 0; i < threads; i++)
                    {
                        // a thread gets ration[i] nodes starting from currentNode
                        threadStarts.Add(PrepareAnalyseInSingleThread(currentNode, currentNode + ration[i] - 1));
                        currentNode += ration[i];
                    }
                }
            }
            return threadStarts;
        }


        private int[] Schedule(int cells, int resources)
        {
            int[] rations = new int[cells];
            // each cell gets at least the floor of resources / cells
            int avg = resources / cells;        // Math.floor = integer division
            // 'border' amount of cells get one resource more - this is what remains after division
            int border = resources % cells;
            // fulfill rations array
            for (int i = 0; i < border; i++)
            {
                rations[i] = avg + 1;
            }
            for (int i = border; i < cells; i++)
            {
                rations[i] = avg;
            }
            return rations;
        }



        //public List<ThreadStart> AnalyseWithScheduling(int threads)
        ///* the method divides the tree on parts that need approximately even amount of time
        // * to be evaluated. Then creates a delegate (ThreadStart) for each of the parts and returns
        // * the list with delegates to be called.
        // */
        //{
        //    List<ThreadStart> threadStarts = new List<ThreadStart>();
        //    Analyse();
        //    if (Children != null)
        //    {
        //        int toIndex = -1;
        //        for (int i = Children.Length - 1; i >= 0; i--)
        //        {
        //            // avg number of threads for the nodes
        //            int avg = (int)Math.Round((double)threads / (i + 1));
        //            threads -= avg;
        //            if (threads == 0)
        //            {
        //                if (toIndex == -1)
        //                {
        //                    toIndex = i;
        //                }
        //                // if there are no more threads available, then the only left thread has to
        //                // evaluate all the left nodes and the loop should finish - so the loop variable
        //                // 'i' is set to 0, so that there will not be more iterations
        //                i = 0;
        //            }
        //            switch (avg)
        //            {
        //                // if avarage is 0 then current node may not be assigned to any thread
        //                case 0:
        //                    {
        //                        if (toIndex == -1)
        //                        {
        //                            toIndex = i;
        //                        }
        //                        break;
        //                    }
        //                case 1:
        //                    // if the average is 0 then the single thread should be assigned to the nodes
        //                    {
        //                        if (toIndex == -1)
        //                        {
        //                            threadStarts.Add(this.PrepareAnalyseInSingleThread(i, i));
        //                        }
        //                        else
        //                        {
        //                            // if there were some previous nodes with no assigned threads they are
        //                            // assigned to new thread now
        //                            threadStarts.Add(this.PrepareAnalyseInSingleThread(i, toIndex));
        //                            toIndex = -1;
        //                        }
        //                        break;
        //                    }
        //                default:
        //                    {
        //                        // if avarage is more than 1 then scheduling should be called recursively
        //                        threadStarts.AddRange(Children[i].AnalyseWithScheduling(avg));
        //                        break;
        //                    }
        //            }
        //        }
        //    }
        //    return threadStarts;
        //}


        private ThreadStart PrepareAnalyseInSingleThread(int fromIndex, int toIndex)
            // the method prepares a ThreadStart method to be called in a new thread
        {
            return () => AnalyseInSingleThread(fromIndex, toIndex);
        }


        private void AnalyseInSingleThread(int fromIndex, int toIndex)
            // method analysing and evaluating some of the tree nodes - the nodes from 'fromIndex'
            // to 'toIndex' in Children array of current node
        {
            for (int i = fromIndex; i <= toIndex; i++)
            {
                int childEval = Children[i].Evaluate();
                if (childEval == 1 && Level % 2 == 0 || childEval == -1 && Level % 2 == 1)
                {
                    // alpha-beta pruning!
                    Evaluation = childEval;
                    break;
                }
            }
        }



        public virtual void Analyse()
            /*
             * the method checks if the game is finished - if so it is evaluated
             * or creates new Nodes (Children)
             */
        {
            byte[] moves = Game.Analyse();
            if (moves == null)
            {
                // if the game is finished (moves == null) new Nodes are not created
                Evaluation = Game.Evaluation();
                return;
            }
            /*
             * 'moves' contains 4 bytes for each move:
             * byte 0: source row,
             * byte 1: source column,
             * byte 2: destination row,
             * byte 3: destination column
             * so total size of moves is 4 * number of new Nodes
             */
            Children = new Node[moves.Length / 4];
            for (int i = 0; i < Children.Length; i++)
            {
                Children[i] = new Node(Game.MakeMove(moves[4 * i], moves[4 * i + 1], moves[4 * i + 2], moves[4 * i + 3]), Level + 1);
            }
        }



        public int Evaluate()
            // evaluates current node. If this is not possible depending on current game position
            // then the method is called recursively.
        {
            Analyse();
            if (Children != null)       // if no children then the position is evaluated = return!
            {
                int currentEval;
                int maxEval;
                if (Level % 2 == 0)
                {
                    // MAX level
                    currentEval = -1;
                    maxEval = 1;
                }
                else
                {
                    // MIN level
                    currentEval = 1;
                    maxEval = -1;
                }
                foreach (Node child in Children)
                {
                    int childEvaluation = child.Evaluate();
                    if (childEvaluation == maxEval)
                    {
                        currentEval = maxEval;
                        // alpha-beta pruning!
                        break;
                    }
                    else
                    {
                        if (childEvaluation == 0)
                        // it works only for the games with 3 possible results (-1, 0, 1)
                        // if there are more then from all the Children evaluations should be
                        // chosen the best one (minimum or maximum depending on the node level)
                        {
                            currentEval = 0;
                        }
                    }
                }
                Evaluation = currentEval;
            }
            Children = null;
            return (int)Evaluation;
        }


        public void FinishEvaluating()
            /*
             * this method may called when AT LEAST ALL THE LEAVES in tree ARE EVALUATED!!
             * if they are not then it leads to StackOverflowException!
             * then the method Evaluate should be called instead
             * the method uses MinMax algorithm and evaluates all the other Nodes (should be called
             * for the Root node - then the whole tree gets evaluated)
             */

        {
            if (Evaluation != null)
            {
                return;
            }
            int maxEval;
            if (Level % 2 == 0)
            {
                // MAX level
                maxEval = 1;
                Evaluation = -1;
            }
            else
            {
                // MIN level
                Evaluation = 1;
                maxEval = -1;
            }
            foreach (Node child in Children)
            {
                if (child.Evaluation == null)
                {
                    child.FinishEvaluating();
                }
                if (child.Evaluation == maxEval)
                {
                    // alpha-beta pruning
                    Evaluation = maxEval;
                    break;
                }
                else
                {
                    if (child.Evaluation == 0)
                        // it works only for the games with 3 possible results (-1, 0, 1)
                        // if there are more then from all the Children evaluations should be
                        // chosen the best one (minimum or maximum depending on the node level)
                    {
                        Evaluation = 0;
                    }
                }
            }
        }

    }
}
