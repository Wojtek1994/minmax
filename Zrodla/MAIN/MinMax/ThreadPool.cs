﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace MinMax.MinMax
{
    class ThreadPool
        // class for managing threads. Threads may be added and started. After all the threads finish
        // the event 'PoolFinished' is fired.
    {

        List<Thread> threads;

        private int counter;

        private bool threadsWorking;

        public event EventHandler PoolFinished;

        public ThreadPool()
        {
            threads = new List<Thread>();
            counter = 0;
            threadsWorking = false;
        }

        
        public void Add(ThreadStart startMethod)
            // adds new thread with a callback (ThreadFinished) fired when a thread finishes
        {
            if (!threadsWorking)
            {
                Interlocked.Increment(ref counter);
                // increasing counter does not need to be atomic, because the threads list (threads)
                // is not a concurrent one
                Thread thread = new Thread(() =>
                {
                    startMethod.Invoke();
                    ThreadFinished();           // callback!
                });
                thread.IsBackground = true;          // setting this property to make sure that the application
                                                    // closes correctly, even if the tree is being analysed
                threads.Add(thread);
            }
        }


        public void Add(List<ThreadStart> startMethods)
        {
            if (!threadsWorking && startMethods != null)
            {
                foreach (ThreadStart startMethod in startMethods)
                {
                    Add(startMethod);
                }
            }
        }


        public void Start()
            // starting all the threads in the pool
            // if there are no threads in the pool the event is fired at the spot
        {
            if (!threadsWorking)
            {
                if (threads.Count == 0)
                {
                    OnPoolFinished(EventArgs.Empty);
                    return;
                }
                threadsWorking = true;

                // now run all the threads. The following code does the same as foreach loop which cannot be used here
                // because foreach iteration is not thread-safe, it means that if any thread is modified then
                // the iterated collection is modified and enumaration (foreach) throws an exception
                for (int i = 0; i < threads.Count; i++)
                {
                    threads[i].Start();
                }
            }
        }


        public void ThreadFinished()
            // callback method - after each thread end
            // when there are no threads in the pool ongoing (counter == 0) the PoolFinished event is fired
        {
            if (threadsWorking)
            {
                if (Interlocked.Decrement(ref counter) == 0)
                {
                    threadsWorking = false;
                    OnPoolFinished(EventArgs.Empty);
                }
            }
        }


        protected void OnPoolFinished(EventArgs args)
            // fire event
        {
            if (PoolFinished != null)
            {
                PoolFinished(this, args);
            }
        }



        public void Abort()
        {
            if (threadsWorking)
            {
                for (int i = 0; i < threads.Count; i++)     // abort threads in thread-safe iteration (not foreach)
                {
                    if (threads[i].IsAlive)
                    {
                        threads[i].Abort();
                    }
                }
                threadsWorking = false;
            }
        }
    }
}
