﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace MinMax.MinMax
{

    class GameTree
    {

        private int threadsNumber;

        private Root root;
        public Root Root
        {
            get { return root; }
            set { root = value; }
        }
        
        
        private ThreadPool threadPool;
        public event EventHandler MinMaxFinished;


        public GameTree(Root r, int threads)
        {
            root = r;
            if (threads < 1)
            {
                /*
                 * the default value (number of logical processors) is set when the given value is inappropriate
                 */
                threadsNumber = Environment.ProcessorCount;
            }
            else
            {
                threadsNumber = threads;
            }
            threadPool = new ThreadPool();
            // when threads finish their work the FinishEvaluating method will be called
            // Because the tree is divided into some parts, then the root is never evaluated
            // by any of the threads. After the threads finish their work the FinishEvaluating
            // method will be called
            threadPool.PoolFinished += new EventHandler(FinishEvaluating);
        }


        public void MinMax()
        {
            // scheduling and starting the threads.
            if (root != null)
            {
                List<ThreadStart> startMethods = root.AnalyseWithScheduling(threadsNumber);
                threadPool.Add(startMethods);
                root.SaveMoves();
                threadPool.Start();
            }
        }


        private void FinishEvaluating(object sender, EventArgs args)
        {
            // finish evatuating and fire event MinMaxFinished when evaluating is finished
            threadPool.PoolFinished -= FinishEvaluating;
            root.FinishEvaluating();
            OnMinMaxFinished(EventArgs.Empty);
        }


        public void AbortMinMax()
        {
            threadPool.PoolFinished -= FinishEvaluating;        // delete an event
            threadPool.Abort();         // aborting threads..
        }


        private void OnMinMaxFinished(EventArgs args)
        {
            if (MinMaxFinished != null)
            {
                MinMaxFinished(this, args);
            }
        }


    }
}
