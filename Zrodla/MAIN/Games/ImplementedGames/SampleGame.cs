﻿
using MinMax.Games.Model;


namespace MinMax.Games.ImplementedGames
{

    // simple sample game just to test the tree minmax algorithm

    class SampleGame : Game
    {

        int position;
        const int eye = 12;
        private bool onMove;
        private int? evaluation;

        private int numberOfMoves = 4;

        public SampleGame()
        {
            evaluation = null;
            SetStartpos();
        }


        public SampleGame(int pos, bool om)
        {
            evaluation = null;
            position = pos;
            onMove = om;
        }


        public bool OnMove()
        {
            return onMove;
        }


        public void SetStartpos()
        {
            position = 0;
            onMove = true;
        }




        public byte[] Analyse()
        {
            if (position == eye)
            {
                evaluation = 0;
                return null;
            }
            else
            {
                byte[] arr = new byte[numberOfMoves * 4];
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = 1;
                }
                return arr;
            }
        }

        public int? Evaluation()
        {
            return evaluation;
        }

        public Game MakeMove(byte x0, byte y0, byte x1, byte y1)
        {
            return new SampleGame(position + x0, !onMove);
        }

        public bool CheckMove(byte x0, byte y0, byte x1, byte y1)
        {
            return x0 == 1;
        }

        public void SetPosition(byte[][] pos, bool onMove)
        {
            position = 0;
        }
    }
}
