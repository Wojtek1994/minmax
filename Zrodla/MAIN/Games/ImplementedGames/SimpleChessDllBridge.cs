﻿using System;

using MinMax.Games.Model;
using MinMax.Games.Utils;
using System.Runtime.InteropServices;

namespace MinMax.Games.ImplementedGames
{
    class SimpleChessDllBridge : Game, LibraryClient
    {

        private const string namespaceNameCS = "SimpleChessCS";
        private const string classNameCS = "SimpleChess";

        private delegate IntPtr AnalyseAsm(IntPtr pos, byte rows, byte cols, bool onMove, IntPtr counter);
        private delegate IntPtr AnalyseCS(byte[][] pos, byte rows, byte cols, bool onMove, ref int counter);

        private delegate bool CheckMoveAsm(IntPtr pos, byte rows, byte cols, byte y0, byte x0, byte y1, byte x1, bool onMove);
        private delegate bool CheckMoveCS(byte[][] pos, byte rows, byte cols, byte y0, byte x0, byte y1, byte x1, bool onMove);

        private byte[][] position;
        private int? evaluation;

        public SimpleChessDllBridge()
        {
        }


        private bool onMove;

        public bool OnMove()
        {
            return onMove;
        }

    
        // method calls an Analyse method from proper dll (using LibraryLoader)
        // it uses COM memory allocations so if the thread is aborted all the memory has to be freed
        // thats why almost all the body must be in try-catch-finally block
        public byte[] Analyse()
        {
            byte[] moves;
            IntPtr result = IntPtr.Zero;
            IntPtr counter = IntPtr.Zero;
            IntPtr marshalled2DArray = IntPtr.Zero;
            int length = 0;
            LibraryLoader loader = LibraryLoader.Loader;
            try
            {
                if (loader.IsAssembler)
                {
                    AnalyseAsm analyseDel = (AnalyseAsm)loader.GetAsmMethod("Analyse", typeof(AnalyseAsm));
                    
                    // alloc memory for marshaled arguments (2D byte array and pointer to int variable - counter)
                    marshalled2DArray = ExtendedMarshaling.GetIntPtrForArray(position);
                    counter = Marshal.AllocCoTaskMem(sizeof(int));

                    // get results from function
                    result = analyseDel(marshalled2DArray, (byte)position.Length, (byte)position[0].Length, onMove, counter);
                    length = Marshal.ReadInt32(counter);
                }
                else
                {
                    AnalyseCS analyseDel = (AnalyseCS)loader.GetCSMethod(namespaceNameCS, classNameCS, "Analyse", typeof(AnalyseCS));
                    result = analyseDel(position, (byte)position.Length, (byte)position[0].Length, onMove, ref length);
                }

                if (length == 0)
                    // there are no moves possible - result stores the evaluation
                {
                    evaluation = (int)result;
                    result = IntPtr.Zero;           // not to call exception in finally clause
                    return null;
                }
                else
                {
                    // there are some possible moves - get them from IntPtr and free memory
                    moves = new byte[4 * length];
                    Marshal.Copy(result, moves, 0, 4 * length);
                    return moves;
                }

            }
            catch (System.Threading.ThreadAbortException)
            // caught when aborting the thread
            {
                // the compiler needs the following line (because all paths must return value).
                // In fact, the ThreadAbortException is rethrown (if the ResetAbort is not called) at the end of
                // finally block, so the line is not really essential.
                return null;
            }
            finally
            {
                // freeing COM memory. If exception was not caught then 'result' is freed later
                if (loader.IsAssembler)
                {
                    Marshal.FreeCoTaskMem(counter);
                    ExtendedMarshaling.Free2DByteArray(marshalled2DArray);
                }
                Marshal.FreeCoTaskMem(result);
            }
        }


        public int? Evaluation()
        {
            return evaluation;
        }


        public Game MakeMove(byte y0, byte x0, byte y1, byte x1)
            /*
             * the method copies the current position and 'makes the move' and returns
             * new 2D array with position and changed player on move
             */
        {
            SimpleChessDllBridge sc = new SimpleChessDllBridge();
            byte[][] arr = new byte[position.Length][];
            // copying..
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = new byte[position[i].Length];
                for (int j = 0; j < position[i].Length; j++)
                {
                    arr[i][j] = position[i][j];
                }
            }

            arr[y1][x1] = arr[y0][x0];
            arr[y0][x0] = 0;
            sc.SetPosition(arr, !onMove);
            return sc;
        }


        public bool CheckMove(byte y0, byte x0, byte y1, byte x1)
        {
            LibraryLoader loader = LibraryLoader.Loader;
            if (loader.IsAssembler)
            {
                CheckMoveAsm checkMoveDel = (CheckMoveAsm)loader.GetAsmMethod("CheckMove", typeof(CheckMoveAsm));
                return checkMoveDel(ExtendedMarshaling.GetIntPtrForArray(position), (byte)position.Length,
                                    (byte)position[0].Length, y0, x0, y1, x1, onMove);
            }
            else
            {
                CheckMoveCS checkMoveDel = (CheckMoveCS)loader.GetCSMethod(namespaceNameCS, classNameCS, "CheckMove", typeof(CheckMoveCS));
                return checkMoveDel(position, (byte)position.Length, (byte)position[0].Length, y0, x0, y1, x1, onMove);
            }
        }


        public void SetPosition(byte[][] pos, bool next)
        {
            position = pos;
            onMove = next;
        }



        public void CacheMethods(LibraryLoader loader)
        // caching methods, called by LibraryLoader while setting up a library
        {
            if (loader.IsAssembler)
            {
                loader.CacheAsmMethod("Analyse", typeof(AnalyseAsm));
            }
            else
            {
                loader.CacheCSMethod(namespaceNameCS, classNameCS, "Analyse", typeof(AnalyseCS));
            }
        }
    }
}
