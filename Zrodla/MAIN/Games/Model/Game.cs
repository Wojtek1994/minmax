﻿
namespace MinMax.Games.Model
{
    interface Game
    {
        bool OnMove();

        // returning array of possible moves where each move is defined by 4 consecutive bytes
        byte[] Analyse();

        int? Evaluation();

        Game MakeMove(byte y0, byte x0, byte y1, byte x1);

        bool CheckMove(byte y0, byte x0, byte y1, byte x1);

        void SetPosition(byte[][] pos, bool onMove);
    }
}
