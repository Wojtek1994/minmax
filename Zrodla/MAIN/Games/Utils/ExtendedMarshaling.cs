﻿using System;
using System.Runtime.InteropServices;

namespace MinMax.Games.Utils
{
    static class ExtendedMarshaling
    {

        public static IntPtr GetIntPtrForArray(byte[][] arr)
            // method marshaling the byte 2D array to IntPtr (so that it may be passed to
            // any unmanaged code)

            // condition: all of the subarrays must have equal length!!
        {
            IntPtr addresses = Marshal.AllocCoTaskMem(4 * arr.Length);
            IntPtr values = Marshal.AllocCoTaskMem(arr.Length * arr[0].Length);
            for (int i = 0; i < arr.Length; i++)
            {
                Marshal.Copy(arr[i], 0, values + i * arr[0].Length, arr[i].Length);
                Marshal.WriteInt32(addresses + i * 4, values.ToInt32() + i * arr[0].Length);
            }
            return addresses;
        }


        public static void Free2DByteArray(IntPtr arrPtr)
        // free memory after 2D array (allocated with GetIntPtrForArray)
        {
            if (arrPtr != IntPtr.Zero)
            {
                IntPtr valuesPtr = (IntPtr)Marshal.ReadInt32(arrPtr);
                Marshal.FreeCoTaskMem(valuesPtr);
                Marshal.FreeCoTaskMem(arrPtr);
            }
        }

    }
}
