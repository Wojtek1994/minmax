﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;



namespace MinMax.Games.Utils
{

    // a singleton class LibraryLoader
    class LibraryLoader
    {

        private static bool assembler;
        private static string loadedLibraryName;
        private static IntPtr dllPtrAsm;
        private static Assembly dllPtrCS;
        private static Dictionary<string, Delegate> methodsCache;
        private static LibraryLoader loader;

        private LibraryLoader()
        // singleton class does not have public constructor.
        {
            // freeing library when application exit
            AppDomain.CurrentDomain.ProcessExit += FreeLibraryLoader;
            methodsCache = new Dictionary<string, Delegate>();
        }
        
        public bool IsAssembler
        // defines if current library is written in assembly
        {
            get { return assembler; }
        }


        public static LibraryLoader Loader
        {
            // not as classic singleton - calling constructor if loader == null
            // the loader has to be created via SetLibrary function.
            get
            {
                return loader;      // may return null
            }
        }


        public static void SetLibrary(string libraryName, bool asm, LibraryClient client)
        // sets the library and the assembler property
        {
            // if it is called for the first time then the singleton instance is created
            if (loader == null)
            {
                loader = new LibraryLoader();
            }
            assembler = asm;
            
            if (asm)
            {
                if (libraryName != loadedLibraryName || dllPtrAsm == IntPtr.Zero)
                // if it is another asm library or before the library couldnt be loaded (then try again)
                {
                    loadedLibraryName = libraryName;
                    methodsCache.Clear();
                    FreeLibraryLoader(null, null);
                    // load library
                    dllPtrAsm = LoadLibrary(libraryName);
                    if (dllPtrAsm == IntPtr.Zero)
                    {
                        throw new LibraryLoaderException("Library: \"" + libraryName + "\"could not be loaded (or it is not assembly dll)");
                    }
                }
            }
            else
            {
                if (libraryName != loadedLibraryName || dllPtrCS == null)
                // if it is another asm library or before the library couldnt be loaded (then try again)
                {
                    loadedLibraryName = libraryName;
                    methodsCache.Clear();
                    FreeLibraryLoader(null, null);
                    // load library
                    try
                    {
                        if (!File.Exists(libraryName))
                        {
                            dllPtrCS = null;
                            throw new LibraryLoaderException("Library: \"" + libraryName + "\"could not be loaded (or it is not C# dll)");
                        }
                        dllPtrCS = Assembly.LoadFile(libraryName);
                    }
                    catch (Exception)
                    // possible exceptions:
                    // ArgumentException | ArgumentNullException | System.IO.FileLoadException |
                    // System.IO.FileNotFoundException | System.BadImageFormatException
                    {
                        throw new LibraryLoaderException("Library: \"" + libraryName + "\"could not be loaded (or it is not C# dll)");
                    }
                }
            }

            // caching new methods
            if (client != null)
            {
                client.CacheMethods(loader);
            }
        }


        private static Delegate LoadAsmMethod(string methodName, Type type)
        /*
         * loads procedure from assembly library by the given name
         */
        {
            try
            {
                var procedurePtr = GetProcAddress(dllPtrAsm, methodName);
                return Marshal.GetDelegateForFunctionPointer(procedurePtr, type);
            }
            catch (Exception)
            {
                // any exception thrown while loading a method generates a new loading exception
                throw new LibraryLoaderException("Library missing method name: \"" + methodName + "\" "
                    + "or the method has not proper declaration.");
            }

        }


        private static Delegate LoadCSMethod(string namespaceName, string className, string methodName, Type type)
        /*
         * loading the method from C# library by the given namespace, class name and method name.
         * The type of the method (delegate) must be defined by type argument.
         */
        {
            try
            {
                var classPtr = dllPtrCS.GetType(namespaceName + "." + className);
                var method = classPtr.GetMethod(methodName);
                return method.CreateDelegate(type);
            }
            catch (Exception)
            {
                // any exception thrown while loading a method generates a new loading exception
                throw new LibraryLoaderException("Library missing method name: \"" + methodName + "\" "
                    + "or the method has not proper declaration.");
            }
        }


        public Delegate GetAsmMethod(string methodName, Type type)
        // gets the assembly method. If possible - gets it out of cache
        {
            Delegate del;
            if (methodsCache.TryGetValue(methodName, out del))
            {
                return del;
            }
            else
            {
                del = LoadAsmMethod(methodName, type);
                return del;
            }
        }

        public Delegate GetCSMethod(string namespaceName, string className, string methodName, Type type)
        // gets the C# method. If possible - gets it out of cache
        {
            Delegate del;
            if (methodsCache.TryGetValue(methodName, out del))
            {
                return del;
            }
            else
            {
                del = LoadCSMethod(namespaceName, className, methodName, type);
                return del;
            }
        }


        public void CacheAsmMethod(string methodName, Type type)
        // caches an assembly method from library
        {
            Delegate del = LoadAsmMethod(methodName, type);
            try
            {
                methodsCache.Add(methodName, del);
            }
            catch (ArgumentException)
            {
                // thrown if the method methodName already is in cache
                // cannot add this method again
            }
        }


        public void CacheCSMethod(string namespaceName, string className, string methodName, Type type)
        // caches a C# method from library
        {
            Delegate del = LoadCSMethod(namespaceName, className, methodName, type);
            try
            {
                methodsCache.Add(methodName, del);
            }
            catch (ArgumentException)
            {
                // thrown if the method methodName already is in cache
                // cannot add this method again
            }
        }


        public static void FreeLibraryLoader(object sender, EventArgs e)
        // freeing library (called i.e. on application exit)
        {
            if (dllPtrAsm != IntPtr.Zero)
            {
                FreeLibrary(dllPtrAsm);
                dllPtrAsm = IntPtr.Zero;
            }
            if (dllPtrCS != null)
            {
                dllPtrCS = null;
            }
        }



        [DllImport("kernel32.dll")]
        public static extern IntPtr LoadLibrary(string dllToLoad);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetProcAddress(IntPtr hModule, string procedureName);

        [DllImport("kernel32.dll")]
        public static extern bool FreeLibrary(IntPtr hModule);



    }
}
