﻿using System;

namespace MinMax.Games.Utils
{
    class LibraryLoaderException : Exception
    {

        public LibraryLoaderException() { }

        public LibraryLoaderException(string message)
            : base(message)
        {
            
        }

    }
}
