﻿using System.Drawing;
using System.Windows.Forms;

namespace MinMax.GUI.NewControls
{
    class ChessPictureBox : PictureBox
    {
        public static Image whitePawnImage;
        public static Image blackPawnImage;

        private static int dimension = 80;

        public static int Dimension
        {
            get { return dimension; }
            private set {}
        }

        private static ChessPictureBox checkedPictureBox;
        public static ChessPictureBox CheckedPictureBox
        {
            get { return ChessPictureBox.checkedPictureBox; }
            set { ChessPictureBox.checkedPictureBox = value; }
        }

        private static Color lightSquare = Color.FromArgb(240, 217, 181);
        private static Color darkSquare = Color.FromArgb(181, 136, 99);
        private static Color checkedSquare = Color.Red;

        private int value;
        public int Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        private int row;
        public int Row
        {
            get { return row; }
            set { row = value; }
        }

        private int column;
        public int Column
        {
            get { return column; }
            set { column = value; }
        }


        public ChessPictureBox(int y, int x)
            : base()
        {
            Value = 0;
            Row = y;
            Column = x;
            checkedPictureBox = null;
            // defining colors
            if ((x + y) % 2 == 0)
            {
                BackColor = lightSquare;
            }
            else
            {
                BackColor = darkSquare;
            }
            this.Size = new Size(dimension, dimension);
            this.Location = new Point(x * Width, y * Height);
        }


        public void Check(bool check)
        {
            // if check == true than check picture box (set back color)
            // if check == false then restore default color
            if (check)
            {
                BackColor = checkedSquare;
                CheckedPictureBox = this;
            }
            else
            {
                if ((Row + Column) % 2 == 0)
                {
                    BackColor = lightSquare;
                }
                else
                {
                    BackColor = darkSquare;
                }
                CheckedPictureBox = null;
            }
        }


        public void SetImage(byte picture)
        {
            Value = picture;
            switch (picture)
            {
                case 0:
                    {
                        this.Image = null;
                        break;
                    }
                case 1:
                    {
                        this.Image = whitePawnImage;
                        break;
                    }
                case 2:
                    {
                        this.Image = blackPawnImage;
                        break;
                    }
            }
        }
    }
}
