﻿using System;


namespace MinMax.GUI
{
    partial class MinMaxGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GroupBDll = new System.Windows.Forms.GroupBox();
            this.ButtonLoadCS = new System.Windows.Forms.Button();
            this.ButtonLoadAsm = new System.Windows.Forms.Button();
            this.RadioBCS = new System.Windows.Forms.RadioButton();
            this.RadioBAsm = new System.Windows.Forms.RadioButton();
            this.GroupBThreads = new System.Windows.Forms.GroupBox();
            this.NumericThreads = new System.Windows.Forms.NumericUpDown();
            this.CheckBThreads = new System.Windows.Forms.CheckBox();
            this.LabelThreads = new System.Windows.Forms.Label();
            this.StartButton = new System.Windows.Forms.Button();
            this.ChessBoardManager = new System.Windows.Forms.Panel();
            this.BoardView = new System.Windows.Forms.Panel();
            this.GroupBBoard = new System.Windows.Forms.GroupBox();
            this.ComboBSize = new System.Windows.Forms.ComboBox();
            this.ButtonBoardRestart = new System.Windows.Forms.Button();
            this.LabelBoardSize = new System.Windows.Forms.Label();
            this.DataGVResults = new System.Windows.Forms.DataGridView();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.From = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.To = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Evaluation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.StatusStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ResultsLabel = new System.Windows.Forms.Label();
            this.AbortButton = new System.Windows.Forms.Button();
            this.EvaluationLabel = new System.Windows.Forms.Label();
            this.FileDialogAsm = new System.Windows.Forms.OpenFileDialog();
            this.FileDialogCS = new System.Windows.Forms.OpenFileDialog();
            this.OnMoveValueLabel = new System.Windows.Forms.Label();
            this.OnMoveLabel = new System.Windows.Forms.Label();
            this.GroupBDll.SuspendLayout();
            this.GroupBThreads.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericThreads)).BeginInit();
            this.ChessBoardManager.SuspendLayout();
            this.GroupBBoard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGVResults)).BeginInit();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBDll
            // 
            this.GroupBDll.Controls.Add(this.ButtonLoadCS);
            this.GroupBDll.Controls.Add(this.ButtonLoadAsm);
            this.GroupBDll.Controls.Add(this.RadioBCS);
            this.GroupBDll.Controls.Add(this.RadioBAsm);
            this.GroupBDll.Location = new System.Drawing.Point(565, 12);
            this.GroupBDll.Name = "GroupBDll";
            this.GroupBDll.Size = new System.Drawing.Size(164, 79);
            this.GroupBDll.TabIndex = 0;
            this.GroupBDll.TabStop = false;
            this.GroupBDll.Text = "Libraries";
            // 
            // ButtonLoadCS
            // 
            this.ButtonLoadCS.Location = new System.Drawing.Point(113, 44);
            this.ButtonLoadCS.Name = "ButtonLoadCS";
            this.ButtonLoadCS.Size = new System.Drawing.Size(39, 23);
            this.ButtonLoadCS.TabIndex = 3;
            this.ButtonLoadCS.Text = "Load";
            this.ButtonLoadCS.UseVisualStyleBackColor = true;
            this.ButtonLoadCS.Click += new System.EventHandler(this.ButtonLoadCS_Click);
            // 
            // ButtonLoadAsm
            // 
            this.ButtonLoadAsm.Location = new System.Drawing.Point(113, 16);
            this.ButtonLoadAsm.Name = "ButtonLoadAsm";
            this.ButtonLoadAsm.Size = new System.Drawing.Size(39, 23);
            this.ButtonLoadAsm.TabIndex = 2;
            this.ButtonLoadAsm.Text = "Load";
            this.ButtonLoadAsm.UseVisualStyleBackColor = true;
            this.ButtonLoadAsm.Click += new System.EventHandler(this.ButtonLoadAsm_Click);
            // 
            // RadioBCS
            // 
            this.RadioBCS.AutoSize = true;
            this.RadioBCS.Location = new System.Drawing.Point(6, 48);
            this.RadioBCS.Name = "RadioBCS";
            this.RadioBCS.Size = new System.Drawing.Size(52, 17);
            this.RadioBCS.TabIndex = 1;
            this.RadioBCS.TabStop = true;
            this.RadioBCS.Text = "C# dll";
            this.RadioBCS.UseVisualStyleBackColor = true;
            // 
            // RadioBAsm
            // 
            this.RadioBAsm.AutoSize = true;
            this.RadioBAsm.Checked = true;
            this.RadioBAsm.Location = new System.Drawing.Point(6, 19);
            this.RadioBAsm.Name = "RadioBAsm";
            this.RadioBAsm.Size = new System.Drawing.Size(82, 17);
            this.RadioBAsm.TabIndex = 0;
            this.RadioBAsm.TabStop = true;
            this.RadioBAsm.Text = "Assembly dll";
            this.RadioBAsm.UseVisualStyleBackColor = true;
            // 
            // GroupBThreads
            // 
            this.GroupBThreads.Controls.Add(this.NumericThreads);
            this.GroupBThreads.Controls.Add(this.CheckBThreads);
            this.GroupBThreads.Controls.Add(this.LabelThreads);
            this.GroupBThreads.Location = new System.Drawing.Point(755, 12);
            this.GroupBThreads.Name = "GroupBThreads";
            this.GroupBThreads.Size = new System.Drawing.Size(164, 79);
            this.GroupBThreads.TabIndex = 1;
            this.GroupBThreads.TabStop = false;
            this.GroupBThreads.Text = "Threads";
            // 
            // NumericThreads
            // 
            this.NumericThreads.Enabled = false;
            this.NumericThreads.Location = new System.Drawing.Point(97, 20);
            this.NumericThreads.Maximum = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.NumericThreads.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumericThreads.Name = "NumericThreads";
            this.NumericThreads.Size = new System.Drawing.Size(51, 20);
            this.NumericThreads.TabIndex = 3;
            this.NumericThreads.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumericThreads.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumericThreads_KeyPress);
            this.NumericThreads.Leave += new System.EventHandler(this.NumericThreads_Leave);
            // 
            // CheckBThreads
            // 
            this.CheckBThreads.AutoSize = true;
            this.CheckBThreads.Checked = true;
            this.CheckBThreads.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBThreads.Location = new System.Drawing.Point(9, 44);
            this.CheckBThreads.Name = "CheckBThreads";
            this.CheckBThreads.Size = new System.Drawing.Size(77, 17);
            this.CheckBThreads.TabIndex = 2;
            this.CheckBThreads.Text = "Set default";
            this.CheckBThreads.UseVisualStyleBackColor = true;
            this.CheckBThreads.CheckedChanged += new System.EventHandler(this.CheckBThreads_CheckedChanged);
            // 
            // LabelThreads
            // 
            this.LabelThreads.AutoSize = true;
            this.LabelThreads.Location = new System.Drawing.Point(6, 22);
            this.LabelThreads.Name = "LabelThreads";
            this.LabelThreads.Size = new System.Drawing.Size(84, 13);
            this.LabelThreads.TabIndex = 0;
            this.LabelThreads.Text = "Threads number";
            // 
            // StartButton
            // 
            this.StartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.StartButton.Location = new System.Drawing.Point(755, 108);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(164, 43);
            this.StartButton.TabIndex = 2;
            this.StartButton.Text = "Start!";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // ChessBoardManager
            // 
            this.ChessBoardManager.Controls.Add(this.BoardView);
            this.ChessBoardManager.Location = new System.Drawing.Point(12, 12);
            this.ChessBoardManager.Name = "ChessBoardManager";
            this.ChessBoardManager.Size = new System.Drawing.Size(520, 520);
            this.ChessBoardManager.TabIndex = 3;
            // 
            // BoardView
            // 
            this.BoardView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoardView.Location = new System.Drawing.Point(100, 100);
            this.BoardView.Name = "BoardView";
            this.BoardView.Size = new System.Drawing.Size(320, 320);
            this.BoardView.TabIndex = 4;
            // 
            // GroupBBoard
            // 
            this.GroupBBoard.Controls.Add(this.ComboBSize);
            this.GroupBBoard.Controls.Add(this.ButtonBoardRestart);
            this.GroupBBoard.Controls.Add(this.LabelBoardSize);
            this.GroupBBoard.Location = new System.Drawing.Point(565, 108);
            this.GroupBBoard.Name = "GroupBBoard";
            this.GroupBBoard.Size = new System.Drawing.Size(164, 89);
            this.GroupBBoard.TabIndex = 2;
            this.GroupBBoard.TabStop = false;
            this.GroupBBoard.Text = "Board size";
            // 
            // ComboBSize
            // 
            this.ComboBSize.BackColor = System.Drawing.SystemColors.Window;
            this.ComboBSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBSize.FormattingEnabled = true;
            this.ComboBSize.IntegralHeight = false;
            this.ComboBSize.ItemHeight = 13;
            this.ComboBSize.Items.AddRange(new object[] {
            "4x4",
            "4x5",
            "4x6",
            "5x4",
            "5x5",
            "5x6",
            "6x4",
            "6x5",
            "6x6"});
            this.ComboBSize.Location = new System.Drawing.Point(12, 51);
            this.ComboBSize.Name = "ComboBSize";
            this.ComboBSize.Size = new System.Drawing.Size(65, 21);
            this.ComboBSize.TabIndex = 2;
            // 
            // ButtonBoardRestart
            // 
            this.ButtonBoardRestart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ButtonBoardRestart.Location = new System.Drawing.Point(83, 50);
            this.ButtonBoardRestart.Name = "ButtonBoardRestart";
            this.ButtonBoardRestart.Size = new System.Drawing.Size(75, 24);
            this.ButtonBoardRestart.TabIndex = 1;
            this.ButtonBoardRestart.Text = "Restart!";
            this.ButtonBoardRestart.UseVisualStyleBackColor = true;
            this.ButtonBoardRestart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ButtonBoardRestart_MouseClick);
            // 
            // LabelBoardSize
            // 
            this.LabelBoardSize.AutoSize = true;
            this.LabelBoardSize.Location = new System.Drawing.Point(9, 23);
            this.LabelBoardSize.Name = "LabelBoardSize";
            this.LabelBoardSize.Size = new System.Drawing.Size(119, 13);
            this.LabelBoardSize.TabIndex = 0;
            this.LabelBoardSize.Text = "Select size (rows x cols)";
            // 
            // DataGVResults
            // 
            this.DataGVResults.AllowUserToAddRows = false;
            this.DataGVResults.AllowUserToDeleteRows = false;
            this.DataGVResults.AllowUserToResizeColumns = false;
            this.DataGVResults.AllowUserToResizeRows = false;
            this.DataGVResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGVResults.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.From,
            this.To,
            this.Evaluation});
            this.DataGVResults.Location = new System.Drawing.Point(562, 290);
            this.DataGVResults.Name = "DataGVResults";
            this.DataGVResults.ReadOnly = true;
            this.DataGVResults.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DataGVResults.Size = new System.Drawing.Size(354, 235);
            this.DataGVResults.TabIndex = 4;
            // 
            // No
            // 
            this.No.HeaderText = "No.";
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.No.Width = 50;
            // 
            // From
            // 
            this.From.HeaderText = "From";
            this.From.Name = "From";
            this.From.ReadOnly = true;
            this.From.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.From.Width = 70;
            // 
            // To
            // 
            this.To.HeaderText = "To";
            this.To.Name = "To";
            this.To.ReadOnly = true;
            this.To.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.To.Width = 70;
            // 
            // Evaluation
            // 
            dataGridViewCellStyle3.NullValue = "Alpha-beta pruning";
            this.Evaluation.DefaultCellStyle = dataGridViewCellStyle3;
            this.Evaluation.HeaderText = "Evaluation";
            this.Evaluation.Name = "Evaluation";
            this.Evaluation.ReadOnly = true;
            this.Evaluation.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Evaluation.Width = 120;
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusStripLabel});
            this.StatusStrip.Location = new System.Drawing.Point(0, 544);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(941, 22);
            this.StatusStrip.SizingGrip = false;
            this.StatusStrip.TabIndex = 5;
            // 
            // StatusStripLabel
            // 
            this.StatusStripLabel.Name = "StatusStripLabel";
            this.StatusStripLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // ResultsLabel
            // 
            this.ResultsLabel.AutoSize = true;
            this.ResultsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ResultsLabel.Location = new System.Drawing.Point(562, 258);
            this.ResultsLabel.Name = "ResultsLabel";
            this.ResultsLabel.Size = new System.Drawing.Size(147, 18);
            this.ResultsLabel.TabIndex = 6;
            this.ResultsLabel.Text = "Results of analyse";
            // 
            // AbortButton
            // 
            this.AbortButton.Enabled = false;
            this.AbortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AbortButton.Location = new System.Drawing.Point(755, 154);
            this.AbortButton.Name = "AbortButton";
            this.AbortButton.Size = new System.Drawing.Size(164, 43);
            this.AbortButton.TabIndex = 7;
            this.AbortButton.Text = "Abort";
            this.AbortButton.UseVisualStyleBackColor = true;
            this.AbortButton.Click += new System.EventHandler(this.AbortButton_Click);
            // 
            // EvaluationLabel
            // 
            this.EvaluationLabel.AutoSize = true;
            this.EvaluationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EvaluationLabel.Location = new System.Drawing.Point(752, 258);
            this.EvaluationLabel.Name = "EvaluationLabel";
            this.EvaluationLabel.Size = new System.Drawing.Size(0, 18);
            this.EvaluationLabel.TabIndex = 8;
            // 
            // OnMoveValueLabel
            // 
            this.OnMoveValueLabel.AutoSize = true;
            this.OnMoveValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.OnMoveValueLabel.Location = new System.Drawing.Point(752, 209);
            this.OnMoveValueLabel.Name = "OnMoveValueLabel";
            this.OnMoveValueLabel.Size = new System.Drawing.Size(0, 15);
            this.OnMoveValueLabel.TabIndex = 10;
            // 
            // OnMoveLabel
            // 
            this.OnMoveLabel.AutoSize = true;
            this.OnMoveLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.OnMoveLabel.Location = new System.Drawing.Point(562, 209);
            this.OnMoveLabel.Name = "OnMoveLabel";
            this.OnMoveLabel.Size = new System.Drawing.Size(105, 15);
            this.OnMoveLabel.TabIndex = 9;
            this.OnMoveLabel.Text = "Player on move";
            // 
            // MinMaxGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 566);
            this.Controls.Add(this.OnMoveValueLabel);
            this.Controls.Add(this.OnMoveLabel);
            this.Controls.Add(this.EvaluationLabel);
            this.Controls.Add(this.AbortButton);
            this.Controls.Add(this.ResultsLabel);
            this.Controls.Add(this.StatusStrip);
            this.Controls.Add(this.DataGVResults);
            this.Controls.Add(this.GroupBBoard);
            this.Controls.Add(this.ChessBoardManager);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.GroupBThreads);
            this.Controls.Add(this.GroupBDll);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MinMaxGUI";
            this.Text = "MinMax";
            this.GroupBDll.ResumeLayout(false);
            this.GroupBDll.PerformLayout();
            this.GroupBThreads.ResumeLayout(false);
            this.GroupBThreads.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericThreads)).EndInit();
            this.ChessBoardManager.ResumeLayout(false);
            this.GroupBBoard.ResumeLayout(false);
            this.GroupBBoard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGVResults)).EndInit();
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBDll;
        private System.Windows.Forms.RadioButton RadioBCS;
        private System.Windows.Forms.RadioButton RadioBAsm;
        private System.Windows.Forms.GroupBox GroupBThreads;
        private System.Windows.Forms.CheckBox CheckBThreads;
        private System.Windows.Forms.Label LabelThreads;
        private System.Windows.Forms.NumericUpDown NumericThreads;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Panel ChessBoardManager;
        private System.Windows.Forms.GroupBox GroupBBoard;
        private System.Windows.Forms.ComboBox ComboBSize;
        private System.Windows.Forms.Button ButtonBoardRestart;
        private System.Windows.Forms.Label LabelBoardSize;
        private System.Windows.Forms.Panel BoardView;
        private System.Windows.Forms.DataGridView DataGVResults;
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel StatusStripLabel;
        private System.Windows.Forms.Label ResultsLabel;
        private System.Windows.Forms.Button AbortButton;
        private System.Windows.Forms.Label EvaluationLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn No;
        private System.Windows.Forms.DataGridViewTextBoxColumn From;
        private System.Windows.Forms.DataGridViewTextBoxColumn To;
        private System.Windows.Forms.DataGridViewTextBoxColumn Evaluation;
        private System.Windows.Forms.OpenFileDialog FileDialogAsm;
        private System.Windows.Forms.Button ButtonLoadCS;
        private System.Windows.Forms.Button ButtonLoadAsm;
        private System.Windows.Forms.OpenFileDialog FileDialogCS;
        private System.Windows.Forms.Label OnMoveValueLabel;
        private System.Windows.Forms.Label OnMoveLabel;
    }
}

