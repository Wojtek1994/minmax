﻿using System;
using System.Drawing;
using System.Windows.Forms;

using MinMax.GUI.NewControls;
using MinMax.Games.ImplementedGames;
using MinMax.Games.Utils;
using System.Threading;
using MinMax.MinMax;
using System.Diagnostics;
using System.IO;


namespace MinMax.GUI
{
    public partial class MinMaxGUI : Form
    {
        private Stopwatch watch;
        private GameTree tree;

        private delegate void UpdateUIMinMaxFinished(Root root);
        private delegate void UpdateUIMinMaxAborted(string message);
        private delegate void PrintMDelegate(string message);

        private bool onMove = true;

        private static string libraryAsmName;
        private static string libraryCSName;
        private bool minmaxOngoing = false;

        private int decorationWidth = 15;

        public MinMaxGUI()
        {
            try
            {
                ChessPictureBox.whitePawnImage = Image.FromFile(Path.Combine(Directory.GetCurrentDirectory(), @"Images\wP.png"));
                ChessPictureBox.blackPawnImage = Image.FromFile(Path.Combine(Directory.GetCurrentDirectory(), @"Images\bP.png"));
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("No images in Images directory.");
                // no images = exit
                System.Environment.Exit(1);
            }
            InitializeComponent();
            NumericThreads.Value = Environment.ProcessorCount;
            RestartPosition(4, 4);          // default board is 4x4
            ComboBSize.SelectedIndex = 0;       // default combo box option
            libraryAsmName = Path.Combine(Directory.GetCurrentDirectory(), "DLL_ASM.dll");
            libraryCSName = Path.Combine(Directory.GetCurrentDirectory(), "DLL_C.dll");
            watch = new Stopwatch();
        }

        private void CheckBThreads_CheckedChanged(object sender, EventArgs e)
        {
            if (NumericThreads.Enabled)
            {
                NumericThreads.Value = Environment.ProcessorCount;
                PrintMessage("Default threads number: " + Environment.ProcessorCount);
            }
            NumericThreads.Enabled = !NumericThreads.Enabled;
        }



        private void NumericThreads_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 44 || e.KeyChar == 45 || e.KeyChar == 46)
                // if key == ',' or key == '-' or key == '.' then change the key to 'a' and let it be handled
                // as illegal character. None of these chars may be put in NumericThreads (unsigned integers only)
            {
                e.KeyChar = 'a';
            }
        }


        private void NumericThreads_Leave(object sender, EventArgs e)
            // prevent leaving field empty (fulfilling it with proper value)
        {
            ((TextBox)NumericThreads.Controls[1]).Text = NumericThreads.Value.ToString();
        }



        private void DrawBoard(int rows, int cols)
        // drawing empty board in BoardView (adding ChessPictureBoxes)
        {
            this.BoardView.Controls.Clear();
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    ChessPictureBox cpb = new ChessPictureBox(i, j);
                    cpb.MouseClick += ChessPictureBox_MouseClick;
                    this.BoardView.Controls.Add(cpb);
                }
            }
            this.BoardView.Size = new Size(ChessPictureBox.Dimension * cols, ChessPictureBox.Dimension * rows);
            this.BoardView.Location = new Point((this.ChessBoardManager.Width - this.BoardView.Width) / 2,
                                                (this.ChessBoardManager.Height - this.BoardView.Height) / 2);
        }



        public void ChessPictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            int pawnValue;
            if (onMove)
            {
                pawnValue = 1;
            }
            else
            {
                pawnValue = 2;
            }
            ChessPictureBox senderBox = sender as ChessPictureBox;
            ChessPictureBox currentlyCheckedBox = ChessPictureBox.CheckedPictureBox;
            if (currentlyCheckedBox != null)
            {
                if (currentlyCheckedBox == senderBox)
                {
                    // uncheck when the same pawn was clicked
                    currentlyCheckedBox.Check(false);
                }
                else
                {
                    if (senderBox.Value == pawnValue)
                    // check another pawn
                    {
                        currentlyCheckedBox.Check(false);
                        senderBox.Check(true);
                    }
                    else
                    // check if the move is possible
                    {
                        SimpleChessDllBridge sc = new SimpleChessDllBridge();
                        sc.SetPosition(GetPositionFromView(), onMove);
                        byte y0 = (byte)currentlyCheckedBox.Row;
                        byte x0 = (byte)currentlyCheckedBox.Column;
                        byte y1 = (byte)senderBox.Row;
                        byte x1 = (byte)senderBox.Column;

                        try
                        {
                            if (RadioBAsm.Checked)
                            {
                                LibraryLoader.SetLibrary(libraryAsmName, true, null);
                            }
                            else
                            {
                                LibraryLoader.SetLibrary(libraryCSName, false, null);
                            }
                            if (sc.CheckMove(y0, x0, y1, x1))
                            {
                                UserMovedPawn(y0, x0, y1, x1);
                                PrintMessage("Correct move! From: " + GetFieldNotation(x0, y0) + ", to: " + GetFieldNotation(x1, y1));
                            }
                            else
                            {
                                PrintMessage("Inorrect move! From: " + GetFieldNotation(x0, y0) + ", to: " + GetFieldNotation(x1, y1));
                            }
                        }
                        catch (LibraryLoaderException ex)
                        {
                            PrintMessage(ex.Message);
                        }
                        finally
                        {
                            currentlyCheckedBox.Check(false);
                            if (!minmaxOngoing)
                            {
                                LibraryLoader.FreeLibraryLoader(this, EventArgs.Empty);
                            }
                        }
                    }
                }                
            }
            else
            {
                // check the field only if there is the right pawn
                if (senderBox.Value == pawnValue)
                {
                    senderBox.Check(true);
                }
            }
        }

        private void UserMovedPawn(byte y0, byte x0, byte y1, byte x1)
        {
            ChessPictureBox sourceCPB = GetChessPictureBox(y0, x0);
            GetChessPictureBox(y1, x1).SetImage((byte)sourceCPB.Value);
            sourceCPB.SetImage(0);
            onMove = !onMove;
            if (onMove)
            {
                OnMoveValueLabel.Text = "White";
            }
            else
            {
                OnMoveValueLabel.Text = "Black";
            }
        }


        private void RenderPawns(byte[][] position)
        // adding images to ChessPictureBoxes in BoardView according to position content
        {
            foreach (ChessPictureBox box in BoardView.Controls)
            {
                box.SetImage(position[box.Row][box.Column]);
            }
        }


        private byte[][] GetPositionFromView()
        // getting 2D byte array representing position from BoardView
        {
            int rows = this.BoardView.Height / ChessPictureBox.Dimension;
            int cols = this.BoardView.Width / ChessPictureBox.Dimension;
            byte[][] position = new byte[rows][];
            for (int i = 0; i < rows; i++)
            {
                position[i] = new byte[cols];
            }
            foreach (ChessPictureBox cpb in BoardView.Controls)
            {
                position[cpb.Row][cpb.Column] = (byte)cpb.Value;
            }
            return position;
        }


        // creates label with specified properties
        private Label CreateLabel(int width, int height, string text)
        {
            Label label = new Label();
            label.Size = new Size(width, height);
            label.Text = text;
            label.Font = new Font(label.Font.Name, 8, FontStyle.Bold);
            label.TextAlign = ContentAlignment.MiddleCenter;
            return label;
        }


        // generates additional Panel describing 'amount' of files
        private Panel GenerateFilesDecoration(int amount)
        {
            Panel panel = new Panel();
            panel.Size = new Size(amount * ChessPictureBox.Dimension + 2 * decorationWidth, decorationWidth);
            panel.BackColor = Color.Tan;
            panel.BorderStyle = BorderStyle.FixedSingle;
            Label label;
            for (int i = 0; i < amount; i++)
            {
                label = CreateLabel(ChessPictureBox.Dimension, decorationWidth, ((char)(i + 65)).ToString());
                label.Location = new Point(decorationWidth + i * ChessPictureBox.Dimension, 0);
                panel.Controls.Add(label);
            }
            return panel;
        }


        // generates additional Panel describing 'amount' of ranks
        private Panel GenerateRanksDecoration(int amount)
        {
            Panel panel = new Panel();
            panel.Size = new Size(decorationWidth, amount * ChessPictureBox.Dimension + 2 * decorationWidth);
            panel.BackColor = Color.Tan;
            panel.BorderStyle = BorderStyle.FixedSingle;
            Label label;
            for (int i = 0; i < amount; i++)
            {
                label = CreateLabel(decorationWidth, ChessPictureBox.Dimension, (i + 1).ToString());
                label.Location = new Point(0, decorationWidth + i * ChessPictureBox.Dimension);
                panel.Controls.Add(label);
            }
            return panel;
        }


        // adding board decoration (files and ranks description)
        private void AddBoardDecoration(int rows, int cols)
        {
            int posX, posY;
            Panel filesDecorationTop = GenerateFilesDecoration(cols);
            Panel filesDecorationBottom = GenerateFilesDecoration(cols);
            Panel ranksDecorationLeft = GenerateRanksDecoration(rows);
            Panel ranksDecorationRight = GenerateRanksDecoration(rows);

            posX = (ChessBoardManager.Size.Width - BoardView.Size.Width) / 2 - decorationWidth;
            posY = (ChessBoardManager.Size.Height - BoardView.Size.Height) / 2 - decorationWidth;
            filesDecorationTop.Location = new Point(posX, posY);
            ChessBoardManager.Controls.Add(filesDecorationTop);

            posY += BoardView.Size.Height + decorationWidth;
            filesDecorationBottom.Location = new Point(posX, posY);
            ChessBoardManager.Controls.Add(filesDecorationBottom);

            posY -= (BoardView.Size.Height + decorationWidth);
            ranksDecorationLeft.Location = new Point(posX, posY);
            ChessBoardManager.Controls.Add(ranksDecorationLeft);

            posX += BoardView.Size.Width + decorationWidth;
            ranksDecorationRight.Location = new Point(posX, posY);
            ChessBoardManager.Controls.Add(ranksDecorationRight);
        }


        private void RestartPosition(int rows, int cols)
        // drawing new board with game starting position
        {
            ChessBoardManager.Controls.Clear();
            ChessBoardManager.Controls.Add(BoardView);
            DrawBoard(rows, cols);
            byte value;
            byte[][] position = new byte[rows][];
            for (int i = 0; i < rows; i++)
            {
                if (i == 0)
                {
                    value = 1;
                }
                else
                {
                    if (i == rows - 1)
                    {
                        value = 2;
                    }
                    else
                    {
                        value = 0;
                    }
                }
                position[i] = new byte[cols];
                for (int j = 0; j < cols; j++)
                {
                    position[i][j] = value;
                }
            }
            RenderPawns(position);
            AddBoardDecoration(rows, cols);
            onMove = true;
            OnMoveValueLabel.Text = "White";
        }



        private Size GetSizeFromComboBox()
        // getting board size according to combo box value
        {
            string text = (string)ComboBSize.SelectedItem;
            return new Size(text[2] - 48, text[0] - 48);                // -48 to get int from char
        }


        private void ButtonBoardRestart_MouseClick(object sender, MouseEventArgs e)
        // restart board
        {
            Size s = GetSizeFromComboBox();
            RestartPosition(s.Height, s.Width);
            PrintMessage("Game restarted successfully, size: " + ComboBSize.Text);
        }


        private ChessPictureBox GetChessPictureBox(int row, int col)
        {
            foreach (ChessPictureBox cpb in BoardView.Controls)
            {
                if (cpb.Column == col && cpb.Row == row)
                {
                    return cpb;
                }
            }
            return null;
        }


        private void PrintMessage(string message)
        {
            StatusStripLabel.Text = message;
        }


        private void StartMinMax(byte[][] position, bool onMove, int threads, bool asm)
        {
            SimpleChessDllBridge scGame = new SimpleChessDllBridge();
            scGame.SetPosition(position, onMove);
            try
            {
                if (asm)
                {
                    LibraryLoader.SetLibrary(libraryAsmName, asm, scGame);
                }
                else
                {
                    LibraryLoader.SetLibrary(libraryCSName, asm, scGame);
                }
            }
            catch (LibraryLoaderException e)
            {
                this.Invoke(new UpdateUIMinMaxAborted(UpdateUIAfterAbort), e.Message);
                return;
            }
            Root root = new Root(scGame);
            tree = new GameTree(root, threads);
            tree.MinMaxFinished += OnMinMaxFinished;
            watch.Restart();
            tree.MinMax();
            minmaxOngoing = true;           // now it can be aborted
        }


        private void OnMinMaxFinished(object sender, EventArgs e)
        {
            watch.Stop();
            tree.MinMaxFinished -= OnMinMaxFinished;
            Root root = tree.Root;
            LibraryLoader.FreeLibraryLoader(this, EventArgs.Empty);
            this.Invoke(new UpdateUIMinMaxFinished(UpdateUIAfterMinMax), root);          // thread safe UI update
            minmaxOngoing = false;
        }



        private void UpdateUIAfterMinMax(Root root)
        {
            // updating datagrid, buttons and statusstrip
            if (root.Children != null)
            {
                for (int i = 0; i < root.Children.Length; i++)
                {
                    DataGVResults.Rows.Add(i + 1, GetFieldNotation(root.MovesArray[4 * i + 1], root.MovesArray[4 * i]),
                        GetFieldNotation(root.MovesArray[4 * i + 3], root.MovesArray[4 * i + 2]), GetEvaluationText(root.Children[i].Evaluation));
                }
            }
            PrintMessage("Analyse finished!   Execution time: " + watch.Elapsed.TotalMilliseconds.ToString() + "ms");
            EvaluationLabel.Text = GetEvaluationText(root.Evaluation);
            AbortButton.Enabled = false;
            StartButton.Enabled = true;
            GroupBDll.Enabled = true;
        }


        private string GetEvaluationText(int? evaluation)
        {
            if (evaluation == null)
            {
                return null;
            }
            switch (evaluation)
            {
                case 0:
                    {
                        return "Draw";
                    }
                case 1:
                    {
                        return "White wins";
                    }
                case -1:
                    {
                        return "Black wins";
                    }
            }
            return "Unknown result";
        }


        private void StartButton_Click(object sender, EventArgs e)
            // updating gui and starting minmax with parameters read from Form
        {
            StartButton.Enabled = false;
            AbortButton.Enabled = true;
            GroupBDll.Enabled = false;
            EvaluationLabel.Text = "";
            this.DataGVResults.Rows.Clear();

            if (RadioBAsm.Checked)
            {
                PrintMessage("Position analyse started!   Used library: Assembly   Threads number: " + NumericThreads.Value + "   Started at: " + DateTime.Now);
            }
            else
            {
                PrintMessage("Position analyse started!   Used library: C#   Threads number: " + NumericThreads.Value + "   Started at: " + DateTime.Now);
            }
            Thread minmax = new Thread(() => StartMinMax(GetPositionFromView(), onMove, (int)NumericThreads.Value, RadioBAsm.Checked));
            minmax.Start();
        }

    
        private void AbortButton_Click(object sender, EventArgs e)
        {
            PrintMessage("Aborting... This may take a while");
            AbortButton.Enabled = false;
            new Thread(StartAbortAction).Start();
        }


        private void StartAbortAction()
        {
            while (!minmaxOngoing)
            {
                // this prevents from aborting minmax when it has not started already - otherwise there would
                // be 2 threads operating on the same data (threadpool, tree, etc.)
            }
            tree.MinMaxFinished -= OnMinMaxFinished;
            // start aborting..
            tree.AbortMinMax();
            LibraryLoader.FreeLibraryLoader(this, EventArgs.Empty);
            this.Invoke(new UpdateUIMinMaxAborted(UpdateUIAfterAbort), "Analyse aborted!");
            minmaxOngoing = false;
        }


        private void UpdateUIAfterAbort(string message)
        {
            AbortButton.Enabled = false;
            StartButton.Enabled = true;
            GroupBDll.Enabled = true;
            PrintMessage(message);
        }


        private void ButtonLoadAsm_Click(object sender, EventArgs e)
        {
            FileDialogAsm.Filter = "Dynamic link libraries (*.dll)|*.dll";
            if (FileDialogAsm.ShowDialog() == DialogResult.OK)
            {
                libraryAsmName = FileDialogAsm.FileName;
                PrintMessage("Operation successful! Current assembly dll: " + libraryAsmName);
            }
            else
            {
                PrintMessage("Operation cancelled. Current assembly dll: " + libraryAsmName);
            }
        }

        private void ButtonLoadCS_Click(object sender, EventArgs e)
        {
            FileDialogCS.Filter = "Dynamic link libraries (*.dll)|*.dll";
            if (FileDialogCS.ShowDialog() == DialogResult.OK)
            {
                libraryCSName = FileDialogCS.FileName;
                PrintMessage("Operation successful! Current C# dll: " + libraryCSName);
            }
            else
            {
                PrintMessage("Operation cancelled. Current C# dll: " + libraryCSName);
            }
        }


        private String GetFieldNotation(byte x, byte y)
        {
            return (char)(x + 65) + (y + 1).ToString();
        }

    }
}
